// 1. In the S16 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the index.js file to the index.html file.



// 3. Create a variable number that will store the value of the number provided by the user via the prompt.

let varNum = parseInt(prompt(`Enter a number: `));
console.log(`The number you provided is ${varNum}.`)



// 4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
// 5. Create a condition that if the current value is less than or equal to 50, stop the loop.
// 6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
// 7. Create another condition that if the current value is divisible by 5, print the number.

for (varNum; varNum > 0; varNum--) {
	
	if (varNum <= 50) {
		console.log(`The current value is at ${varNum}. Terminating the loop.`);
		break;
	}

	if (varNum % 10 === 0) {
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue; 
	}

	if (varNum % 5 === 0) {
		console.log(varNum);
		continue; 
	}

}



// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.

//supercalifragilisticexpialidocious
//sprclfrglstcxpldcs

// let varString = `supercalifragilisticexpialidocious`;



// 9. Create another variable that will store the consonants from the string.
// 10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
// 11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
// 12. Create an else statement that will add the letter to the second variable.

let word = 'supercalifragilisticexpialidocious';
let newWord = '';

for (let i = 0; i < word.length; i++) {

	if (
		word[i].toLowerCase() == `a` ||
		word[i].toLowerCase() == `i` ||
		word[i].toLowerCase() == `e` ||
		word[i].toLowerCase() == `o` ||
		word[i].toLowerCase() == `u`
		) {

		} else {
			newWord += word[i];
		}
}

console.log(word);
console.log(newWord);



/*const word = 'supercalifragilisticexpialidocious';
const vowels = [`a`, `e`, `i`, `o`, `u`];
let newWord = '';

for (const char of word) {
	
	if (vowels.indexOf(char) === -1){
		newWord += char;
	}
}

console.log(newWord);*/

/*var strings = ["bongo drums", "guitar", "flute", "double bass", "xylophone","piano"];
string = strings.map(x=>x.replace( /[aeiou]/g, '' ));
console.log(string);

str = str.replace( /[aeiou]/ig, '' )*/

/*function disemvowel(str) {
  var strArr = str.split('');
  for (var x = 0; x < str.length; x++) {
    var char = str[x].toLowerCase();
    if (char == "a" || char == "e" || char == "i" || char == "o" || char == "u") {
      strArr[x] = '';
    }
  }
  return strArr.join('');
}

disemvowel(`supercalifragilisticexpialidocious`);*/

// 13. Create a git repository named S16.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle